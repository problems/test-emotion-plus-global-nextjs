## Get starter

    npm i 
    npm run dev 

## Problem

I'm trying to use [global-stylesheet][1] and add normalize.css with [emotion][2] but I got

> StyleSheet: `insertRule` accepts only strings.
  Error: StyleSheet: `insertRule` accepts only strings.
      at invariant (/home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/node_modules/styled-jsx/dist/lib/stylesheet.js:274:11)
      at StyleSheet.insertRule (/home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/node_modules/styled-jsx/dist/lib/stylesheet.js:125:7)
      at /home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/node_modules/styled-jsx/dist/stylesheet-registry.js:88:29
      at Array.map (native)
      at StyleSheetRegistry.add (/home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/node_modules/styled-jsx/dist/stylesheet-registry.js:87:27)
      at JSXStyle.componentWillMount (/home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/node_modules/styled-jsx/dist/style.js:58:26)
      at resolve (/home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/node_modules/react-dom/cjs/react-dom-server.node.development.js:2119:12)
      at ReactDOMServerRenderer.render (/home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/node_modules/react-dom/cjs/react-dom-server.node.development.js:2260:22)
      at ReactDOMServerRenderer.read (/home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/node_modules/react-dom/cjs/react-dom-server.node.development.js:2234:19)
      at renderToString (/home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/node_modules/react-dom/cjs/react-dom-server.node.development.js:2501:25)
      
      
[1]: https://github.com/zeit/next.js/tree/canary/examples/with-global-stylesheet-simple
[2]: https://github.com/zeit/next.js/tree/canary/examples/with-emotion