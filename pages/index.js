import normalize from 'normalize.css/normalize.css';
import styled, {hydrate, css, injectGlobal} from 'react-emotion'

// Adds server generated styles to emotion cache.
// '__NEXT_DATA__.ids' is set in '_document.js'
if (typeof window !== 'undefined') {
    hydrate(window.__NEXT_DATA__.ids)
}

export default () => {
    injectGlobal`
        html, body {
          padding: 3rem 1rem;
          margin: 0;
          background: papayawhip;
          min-height: 100%;
          font-family: Helvetica, Arial, sans-serif;
          font-size: 24px;
        }
      `;

    const basicStyles = css`
        background-color: white;
        color: cornflowerblue;
        border: 1px solid lightgreen;
        border-right: none;
        border-bottom: none;
        box-shadow: 5px 5px 0 0 lightgreen, 10px 10px 0 0 lightyellow;
        transition: all 0.1s linear;
        margin: 3rem 0;
        padding: 1rem 0.5rem;
      `;

    const Basic = styled.div`
        ${basicStyles};
      `;

    return (
        <div>
            <Basic>Cool Styles</Basic>
            <h2>Home Page</h2>

            <style jsx global>{normalize}</style>
        </div>
    )
}