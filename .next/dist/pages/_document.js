'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _document = require('next/dist/server/document.js');

var _document2 = _interopRequireDefault(_document);

var _emotionServer = require('emotion-server');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/home/riderman/WebstormProjects/tmp/testEmotionPlusGlobal/pages/_document.js?entry';


var MyDocument = function (_Document) {
    (0, _inherits3.default)(MyDocument, _Document);

    (0, _createClass3.default)(MyDocument, null, [{
        key: 'getInitialProps',
        value: function getInitialProps(_ref) {
            var renderPage = _ref.renderPage;

            var page = renderPage();
            var styles = (0, _emotionServer.extractCritical)(page.html);
            return (0, _extends3.default)({}, page, styles);
        }
    }]);

    function MyDocument(props) {
        (0, _classCallCheck3.default)(this, MyDocument);

        var _this = (0, _possibleConstructorReturn3.default)(this, (MyDocument.__proto__ || (0, _getPrototypeOf2.default)(MyDocument)).call(this, props));

        var __NEXT_DATA__ = props.__NEXT_DATA__,
            ids = props.ids;

        if (ids) {
            __NEXT_DATA__.ids = ids;
        }
        return _this;
    }

    (0, _createClass3.default)(MyDocument, [{
        key: 'render',
        value: function render() {
            return _react2.default.createElement('html', {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 21
                }
            }, _react2.default.createElement(_document.Head, {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 22
                }
            }, _react2.default.createElement('title', {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 23
                }
            }, 'With Emotion'), _react2.default.createElement('style', { dangerouslySetInnerHTML: { __html: this.props.css }, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 24
                }
            })), _react2.default.createElement('body', {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 26
                }
            }, _react2.default.createElement(_document.Main, {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 27
                }
            }), _react2.default.createElement(_document.NextScript, {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 28
                }
            })));
        }
    }]);

    return MyDocument;
}(_document2.default);

exports.default = MyDocument;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL19kb2N1bWVudC5qcyJdLCJuYW1lcyI6WyJEb2N1bWVudCIsIkhlYWQiLCJNYWluIiwiTmV4dFNjcmlwdCIsImV4dHJhY3RDcml0aWNhbCIsIk15RG9jdW1lbnQiLCJyZW5kZXJQYWdlIiwicGFnZSIsInN0eWxlcyIsImh0bWwiLCJwcm9wcyIsIl9fTkVYVF9EQVRBX18iLCJpZHMiLCJfX2h0bWwiLCJjc3MiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxBQUFPLEFBQVksQUFBTSxBQUFNOzs7O0FBQy9CLEFBQVM7Ozs7Ozs7SSxBQUVZOzs7Ozs4Q0FDdUI7Z0JBQWQsQUFBYyxrQkFBZCxBQUFjLEFBQ3BDOztnQkFBTSxPQUFOLEFBQWEsQUFDYjtnQkFBTSxTQUFTLG9DQUFnQixLQUEvQixBQUFlLEFBQXFCLEFBQ3BDOzhDQUFBLEFBQVksTUFBWixBQUFxQixBQUN4QjtBQUVEOzs7d0JBQUEsQUFBYSxPQUFPOzRDQUFBOztrSkFBQSxBQUNWOztZQURVLEFBRVIsZ0JBRlEsQUFFZSxNQUZmLEFBRVI7WUFGUSxBQUVPLE1BRlAsQUFFZSxNQUZmLEFBRU8sQUFDdkI7O1lBQUEsQUFBSSxLQUFLLEFBQ0w7MEJBQUEsQUFBYyxNQUFkLEFBQW9CLEFBQ3ZCO0FBTGU7ZUFNbkI7Ozs7O2lDQUVTLEFBQ047bUNBQ0ksY0FBQTs7OEJBQUE7Z0NBQUEsQUFDQTtBQURBO0FBQUEsYUFBQSxrQkFDQSxBQUFDOzs4QkFBRDtnQ0FBQSxBQUNJO0FBREo7QUFBQSwrQkFDSSxjQUFBOzs4QkFBQTtnQ0FBQTtBQUFBO0FBQUEsZUFESixBQUNJLEFBQ0EsMERBQU8seUJBQXlCLEVBQUUsUUFBUSxLQUFBLEFBQUssTUFBL0MsQUFBZ0MsQUFBcUI7OEJBQXJEO2dDQUhKLEFBQ0EsQUFFSSxBQUVKO0FBRkk7aUNBRUosY0FBQTs7OEJBQUE7Z0NBQUEsQUFDQTtBQURBO0FBQUEsK0JBQ0EsQUFBQzs7OEJBQUQ7Z0NBREEsQUFDQSxBQUNBO0FBREE7QUFBQSxnQ0FDQSxBQUFDOzs4QkFBRDtnQ0FSSixBQUNJLEFBS0EsQUFFQSxBQUlQO0FBSk87QUFBQTs7Ozs7QUF4QjRCLEE7O2tCQUFuQixBIiwiZmlsZSI6Il9kb2N1bWVudC5qcz9lbnRyeSIsInNvdXJjZVJvb3QiOiIvaG9tZS9yaWRlcm1hbi9XZWJzdG9ybVByb2plY3RzL3RtcC90ZXN0RW1vdGlvblBsdXNHbG9iYWwifQ==